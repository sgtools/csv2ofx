# csv2ofx

[![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/sgtools%2Fcsv2ofx?style=flat-square)](https://gitlab.com/sgtools/csv2ofx/-/pipelines)
[![GitLab Release](https://img.shields.io/gitlab/v/release/sgtools%2Fcsv2ofx?style=flat-square)](https://gitlab.com/sgtools/csv2ofx/-/releases)
[![Static Badge](https://img.shields.io/badge/license-GPL%203.0-blue?style=flat-square)](https://gitlab.com/sgtools/csv2ofx/-/blob/main/LICENSE)
[![Static Badge](https://img.shields.io/badge/Open%20Source%3F-Yes!-blue?style=flat-square)](#)

> LaBanquePostale OFX enhancement from CSV exports

I use LaBanquePostale as a bank and OFX downloads have truncated transactions descriptions  
This small tool enhance OFX exports from CSV exports where transactions are more detailed

**Main features**
 - Updates OFX files from CSV exports

*Developped and tested on Ubuntu Jammy 22.04*

## Installation (Ubuntu Jammy 22.04)

```bash
sudo sh -c 'curl -SsL https://sgtools.gitlab.io/ppa/pgp-key.public | gpg --dearmor > /etc/apt/trusted.gpg.d/sgtools.gpg'
sudo sh -c 'curl -SsL -o /etc/apt/sources.list.d/sgtools.list https://sgtools.gitlab.io/ppa/sgtools.list'
sudo apt update
sudo apt install csv2ofx
```

## Usage

```bash
csv2ofx --help
```

See [documentation page](https://sgtools.gitlab.io/csv2ofx)

## Dependencies

- [anyhow](https://crates.io/crates/anyhow)
- [clap](https://crates.io/crates/clap)
- [inquire](https://crates.io/crates/inquire)

## License

Copyright (C) 2024 Sebastien Guerri

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

## Contributors

- Sébastien Guerri: [@sguerri](https://gitlab.com/sguerri)