## What's Changed in v0.1.6 
* ci: Remove unrequired dependencies

## What's Changed in v0.1.5 
* chore: Update tasks and gitlab ci

## What's Changed in 0.1.4 
* refactor: Update tasks
* docs: Update README

## What's Changed in 0.1.1 
* chore: Update settings
* refactor: Add help and version commands

## What's Changed in 0.1.0 
* chore: Add CHANGELOG
* Initial commit

