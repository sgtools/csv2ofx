// csv2ofx - LaBanquePostale OFX enhancement from CSV exports
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::env;
use std::collections::HashMap;
use std::fs;
use std::io::Read;
use std::path::Path;
use std::path::PathBuf;

use clap::Command;
use anyhow::anyhow;
use anyhow::Result;
use inquire::Confirm;

type DataItem = HashMap<String, PathBuf>;

#[derive(Debug)]
struct DataLine
{
    text: String,
    amount: f32,
}

fn get_data(path: &PathBuf) -> Result<(DataItem, DataItem)>
{
    println!("Searching files...");
    let paths = fs::read_dir(path)?.flatten();
    let mut data_csv = HashMap::<String, PathBuf>::new();
    let mut data_ofx = HashMap::<String, PathBuf>::new();
    for path in paths {
        let path = path.path();
        if !path.is_file() { continue; }
        if let Some(extension) = path.extension() {
            if extension.eq("csv") {
                let name = path.file_name().expect("Incorrect file name").to_str().expect("Incorrect file name conversion");
                let key = &name[0..11];
                println!("> Added: {}", path.display());
                data_csv.insert(key.to_owned(), path);
            } else if extension.eq("ofx") {
                let name = path.file_name().expect("Incorrect file name").to_str().expect("Incorrect file name conversion");
                let key = &name[0..11];
                println!("> Added: {}", path.display());
                data_ofx.insert(key.to_owned(), path);
            }
        }
    }
    println!();
    Ok((data_csv, data_ofx))
}

fn read_csv(path_csv: &Path) -> Result<Vec<DataLine>>
{
    let mut data: Vec<DataLine> = vec![];

    let mut buffer = vec![];
    let mut file = fs::File::open(path_csv)?;
    file.read_to_end(&mut buffer)?;
    let content = String::from_utf8_lossy(&buffer);

    let lines: Vec<&str> = content.split("\r\n").collect();
    let lines = &lines[7..];
    for line in lines {
        if line.is_empty() { continue; }
        let items: Vec<&str> = line.split(';').collect();
        if items.len() != 3 { continue; }
        let text = items[1].replace('\"', "");
        let amount = items[2].replace(',', ".").parse::<f32>()?;
        data.push(DataLine { text: text.to_owned(), amount });
    }

    Ok(data)
}

fn read_ofx(path_ofx: &Path) -> Result<Vec<DataLine>>
{
    let mut data: Vec<DataLine> = vec![];

    let mut buffer = vec![];
    let mut file = fs::File::open(path_ofx)?;
    file.read_to_end(&mut buffer)?;
    let content = String::from_utf8_lossy(&buffer);

    let lines: Vec<&str> = content.split('\n').collect();
    for line in lines {
        if !line.starts_with("<OFX>") { continue; }
        let items = line.split("<STMTTRN>");
        for item in items {
            if !item.starts_with("<TRNTYPE>") { continue; }
            let text: Vec<&str> = item.split("<NAME>").collect();
            let text = text[1];
            let text: Vec<&str> = text.split("</STMTTRN>").collect();
            let text = text[0];
            let amount: Vec<&str> = item.split("<TRNAMT>").collect();
            let amount = amount[1];
            let amount: Vec<&str> = amount.split("<FITID>").collect();
            let amount = amount[0].parse::<f32>()?;
            data.push(DataLine { text: text.to_owned(), amount });
        }
    }

    Ok(data)
}

fn run_checks(csv_items: &[DataLine], ofx_items: &[DataLine]) -> Result<()>
// fn run_checks(csv_items: &Vec<DataLine>, ofx_items: &Vec<DataLine>) -> Result<()>
{
    if csv_items.len() != ofx_items.len() {
        Err(anyhow!("Inconsistent number of entries"))?;
    }

    for i in (0..csv_items.len()).collect::<std::vec::Vec<usize>>() {
        let csv_amount = csv_items.get(i).unwrap().amount;
        let ofx_amount = ofx_items.get(i).unwrap().amount;
        if csv_amount != ofx_amount {
            Err(anyhow!("Inconsistent value on entry n°{}: {} vs {}", i, csv_amount, ofx_amount))?;
        }
    }

    Ok(())
}

fn create_new_file(new_path: &str, csv_items: &[DataLine], ofx_items: &[DataLine], path_ofx: &Path) -> Result<()>
{
    let mut new_content: Vec<String> = vec![];

    let mut buffer = vec![];
    let mut file = fs::File::open(path_ofx)?;
    file.read_to_end(&mut buffer)?;
    let content = String::from_utf8_lossy(&buffer);

    let lines: Vec<&str> = content.split('\n').collect();
    for line in lines {
        if line.starts_with("<OFX>") {
            let mut new_line: Vec<&str> = vec![];
            let mut i = 0;
            let items: Vec<&str> = line.split("<STMTTRN>").collect();
            for item in items {
                if item.starts_with("<TRNTYPE>") {
                    let parts: Vec<&str> = item.split(&ofx_items.get(i).unwrap().text).collect();
                    new_line.push("<STMTTRN>");
                    new_line.push(parts[0]);
                    new_line.push(&csv_items.get(i).unwrap().text);
                    new_line.push(parts[1]);
                    i += 1;
                } else {
                    new_line = [ item ].to_vec();
                }
}
            new_content.push(new_line.join(""));
        } else {
            new_content.push(line.to_owned());
        }
    }

    fs::write(new_path, new_content.join("\n"))?;

    Ok(())
}


fn handle_file(key: &str, path_csv: &Path, path_ofx: &Path) -> Result<()>
{
    println!("Handling {}...", key);

    let csv_items = read_csv(path_csv)?;
    println!("> CSV file loaded");

    let ofx_items = read_ofx(path_ofx)?;
    println!("> OFX file loaded");

    run_checks(&csv_items, &ofx_items)?;
    println!("> Checks OK");

    let new_path = path_ofx.display().to_string().replace(".ofx", "_new.ofx");
    create_new_file(&new_path, &csv_items, &ofx_items, path_ofx)?;
    println!("> New files created");

    fs::remove_file(path_csv)?;
    fs::remove_file(path_ofx)?;
    println!("> Cleaned");

    println!();
    Ok(())
}


fn build_cli() -> Command
{
    Command::new(clap::crate_name!())
        .about(clap::crate_description!())
        .version(clap::crate_version!())
}


fn main() -> Result<()>
{
    let path = env::current_dir()?;

    let _ = build_cli().get_matches();

    let ok = Confirm::new("Run csv2ofx in current directory?").with_default(true).prompt();
    if let Ok(true) = ok {
        println!();

        let (data_csv, data_ofx) = get_data(&path)?;
        for (key, path_csv) in data_csv.iter() {
            if let Some(path_ofx) = data_ofx.get(key) {
                handle_file(key, path_csv, path_ofx)?;
            }
        }
    } else {
        println!("> Cancelled");
    }

    Ok(())
}
